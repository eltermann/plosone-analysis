import argparse
from os.path import join

import matplotlib.pyplot as plt
import numpy as np


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--samplesdir', required=True)
    args = parser.parse_args()

    for i in range(1, 19):
        sampleid = 'sample{0}.pic'.format(i)
        data = np.load(join(args.samplesdir, sampleid))
        if data.shape[0] > 0:
            plt.title(sampleid)
            plt.scatter(data[:,0], data[:,1])
            plt.show()
    _ = raw_input('Press Enter to exit')
