# -*- coding: utf-8 -*-
import argparse
import os
from os.path import join

import matplotlib
import matplotlib.pyplot as plt
import numpy as np


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--samplesdir', required=True)
    args = parser.parse_args()

    matplotlib.rcParams.update({'font.size': 14})

    histsdir = join(args.samplesdir, 'histograms')
    if not os.path.exists(histsdir):
        os.makedirs(histsdir)

    for i in [1, 2, 3, 6, 7]:
        data = np.load(join(args.samplesdir, 'sample%d.pic' % (i)))
        ind = 0.5 + 1.1 * np.arange(10)
        width = 0.35
        if data.shape[0] > 0:
            x = data[:,0]
            y = data[:,1]
            total_y_0 = []
            total_y_1 = []
            for j in range(1, 10) + [-1]:
                total_y_0.append(sum(np.logical_and(y == 0, x == j)))
                total_y_1.append(sum(np.logical_and(y == 1, x == j)))
            plt.gcf().clear()
            p1 = plt.bar(ind, total_y_0, width, color='y', log=True)
            p2 = plt.bar(ind + width, total_y_1, width, color='r', log=True)
            xticks = ('1', '2', '3', '4', '5', '6', '7', '8', '9', u'∞')
            plt.xticks(ind + width, xticks)
            plt.legend((p1[0], p2[0]), ('flow does not occur', 'flow occurs'), loc='best')
            plt.xlabel('Social distance')
            fname = join(histsdir, 'sample%d.png' % (i))
            plt.savefig(fname)
            print('Saved to %s' % (fname))
