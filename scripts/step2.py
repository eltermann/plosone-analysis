from os.path import join, dirname
import sys
sys.path.append(join(dirname(__file__), '..'))


import logging
from lxml import etree
import re

import settings
import utils

utils.configure_logging()


if __name__ == '__main__':
    mongodb = utils.mongodb_db()
    mongodb.article.ensure_index([('doi', 1)])
    mongodb.article.ensure_index([('title_hash', 1)])
    mongodb.article.ensure_index([('year', 1)])
    articles_count = 0
    referred_articles_count = 0
    logging.info('Iterating over corpus to load articles into DB')
    for xml in utils.load_corpus():
        try:
            tree = etree.fromstring(xml)
        except Exception as e:
            logging.warning('Unable to parse document: %s', repr(e))
            continue

        # article
        doi = ''
        try:
            doi = tree.xpath(
                '/article/front/article-meta/article-id[@pub-id-type="doi"]'
            )[0].text.strip()
            assert doi, 'DOI unavailable'
        except Exception as e:
            logging.debug('Unable to fetch DOI: %s', repr(e))

        try:
            title = tree.xpath(
                '/article/front/article-meta/title-group/article-title'
            )[0].text
            assert title, 'Title unavailable'
            title_hash = utils._title_hash(utils._canonical_title(title))
        except Exception as e:
            logging.debug('Unable to fetch title: %s', repr(e))
            if not doi:
                logging.warning('Unable to fetch title nor doi; skipping.')
                continue

        try:
            year = tree.xpath('//article-meta/pub-date/year')[0].text.strip()
            year = re.sub('[^0-9]', '', year) # '2005a' -> '2005'
            assert len(year) == 4, u'Invalid year {0}'.format(year)
            year = int(year)
        except Exception as e:
            logging.debug('Unable to fetch year: %s', repr(e))
            continue

        # article authors
        authors = []
        countries = []
        regions = []
        for author_node in tree.xpath('//contrib[@contrib-type="author"]'):
            try:
                name_node = author_node.find('name')
                surname, given_names = utils._extract_author_names(name_node)
                assert surname and given_names, 'Invalid author name'
                name = utils._author_canonical_name(surname, given_names)
            except Exception as e:
                logging.debug('Unable to fetch author name: %s', repr(e))
                continue

            existing_author = mongodb.author.find_one({'canonical_name': name})
            if existing_author:
                authors.append(existing_author['_id'])
                if existing_author.get('country'):
                    countries.append(existing_author.get('country'))
                if existing_author.get('region'):
                    regions.append(existing_author.get('region'))
            else:
                logging.error('Missing author %s', name)
        if len(authors) == 0:
            logging.debug('Unable to fetch authors; skip')
            continue
        first_country = countries[0] if countries else ''
        majority_country = utils._majority(countries) if countries else ''
        first_region = regions[0] if regions else ''
        majority_region = utils._majority(regions) if regions else ''

        # subjects
        subjects = {}
        for subject_group_node in tree.xpath(
            '//article-meta/article-categories'
            '/subj-group[@subj-group-type="Discipline-v3"]'
        ):
            level = 1
            last_names = []
            for subject_name_n in subject_group_node.iterdescendants('subject'):
                name = subject_name_n.text.upper()
                last_names.append(name)
                name = '::'.join(last_names)
                subject_doc = mongodb.subject.find_one({'name': name})
                if subject_doc:
                    if level not in subjects:
                        subjects[level] = []
                    subjects[level].append(subject_doc['_id'])
                else:
                    logging.error('Missing subject %s', name)
                level += 1

        existing_article = utils._existing_article(doi, title_hash)
        if existing_article:
            mongodb.article.update_one(
                {'_id': existing_article['_id']},
                {
                    '$set': {
                        'doi': doi,
                        'title_hash': title_hash,
                        'year': year,
                        'authors': authors,
                        'lv1_subjects': subjects.get(1, []),
                        'lv2_subjects': subjects.get(2, []),
                        'lv3_subjects': subjects.get(3, []),
                        'lv4_subjects': subjects.get(4, []),
                        'lv5_subjects': subjects.get(5, []),
                        'lv6_subjects': subjects.get(6, []),
                        'lv7_subjects': subjects.get(7, []),
                        'first_country': first_country,
                        'majority_country': majority_country,
                        'first_region': first_region,
                        'majority_region': majority_region,

                    }
                }
            )
        else:
            article_doc = {
                'doi': doi,
                'title_hash': title_hash,
                'year': year,
                'authors': authors,
                'lv1_subjects': subjects.get(1, []),
                'lv2_subjects': subjects.get(2, []),
                'lv3_subjects': subjects.get(3, []),
                'lv4_subjects': subjects.get(4, []),
                'lv5_subjects': subjects.get(5, []),
                'lv6_subjects': subjects.get(6, []),
                'lv7_subjects': subjects.get(7, []),
                'first_country': first_country,
                'majority_country': majority_country,
                'first_region': first_region,
                'majority_region': majority_region,
            }
            mongodb.article.insert(article_doc)
            logging.debug(u'Created article %s', doi)
            articles_count += 1

        # referred articles
        for ref_article_n in tree.xpath(
            '/article/back/ref-list/ref/*[@publication-type="journal"]'
        ):
            doi = ''
            try:
                assert 'doi' in ref_article_n.find('comment').text, 'malformed'
                doi = ref_article_n.xpath(
                    './comment/ext-link[@ext-link-type="uri"]'
                )[0].text
            except Exception as e:
                logging.debug('Unable to find DOI (ref): %s', repr(e))

            try:
                title = ref_article_n.find('article-title').text
                assert title, 'Title unavailable (ref)'
                title_hash = utils._title_hash(utils._canonical_title(title))
            except Exception as e:
                logging.debug('Unable to fetch title (ref): %s', repr(e))
                if not doi:
                    logging.warn('Unable to fetch title nor doi; skip (ref).')
                    continue

            try:
                year = ref_article_n.find('year').text
                year = re.sub('[^0-9]', '', year) # '2005a' -> '2005'
                assert len(year) == 4, u'Invalid year {0}'.format(year)
                year = int(year)
            except Exception as e:
                logging.warning('Unable to fetch year (ref): %s', repr(e))
                continue

            authors = []
            countries = []
            regions = []
            for author_name_n in ref_article_n.xpath('.//name'):
                try:
                    surname, given_names = utils._extract_author_names(
                        author_name_n
                    )
                    assert surname and given_names, 'Invalid author name'
                    name = utils._author_canonical_name(surname, given_names)
                except Exception as e:
                    logging.debug(
                        'Unable to fetch author name (ref): %s', repr(e)
                    )
                    continue
                existing_author = mongodb.author.find_one(
                    {'canonical_name': name}
                )
                if existing_author:
                    authors.append(existing_author['_id'])
                    if existing_author.get('country'):
                        countries.append(existing_author.get('country'))
                    if existing_author.get('region'):
                        regions.append(existing_author.get('region'))
                else:
                    logging.error('Missing author %s', name)
            if len(authors) == 0:
                logging.debug('Unable to fetch authors; skip')
                continue
            first_country = countries[0] if countries else ''
            majority_country = utils._majority(countries) if countries else ''
            first_region = regions[0] if regions else ''
            majority_region = utils._majority(regions) if regions else ''

            existing_article = utils._existing_article(doi, title_hash)
            if not existing_article:
                ref_article = {
                    'doi': doi,
                    'title_hash': title_hash,
                    'year': year,
                    'authors': authors,
                    'first_country': first_country,
                    'majority_country': majority_country,
                    'first_region': first_region,
                    'majority_region': majority_region,
                }
                mongodb.article.insert(ref_article)
                referred_articles_count += 1


    logging.info('Created:')
    logging.info('   %d articles', articles_count)
    logging.info('   %d referred articles', referred_articles_count)
