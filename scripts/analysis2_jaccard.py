# -*- coding: utf-8 -*-
from os.path import join, dirname
import sys
sys.path.append(join(dirname(__file__), '..'))

import utils


import argparse
from os.path import join

import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
from sklearn.metrics import jaccard_similarity_score


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--samplesdir', required=True)
    args = parser.parse_args()

    print('                             dist=0 dist=1 dist=2 dist=3 dist=4 dist=5 dist=6 dist=7 dist=8 dist=9 dist=10 dist=∞')
    for i in range(1, 19):
        sampleid = 'sample{0}.pic'.format(i)
        data = np.load(join(args.samplesdir, sampleid))
        if not data.shape[0]:
            continue
        y = data[:, 1]
        X = np.array([utils.dist2fv(x) for x in data[:, 0]])
        coeffs = []
        for feature in range(1 if i <= 9 else 0, 12):
            coeffs.append(jaccard_similarity_score(y, X[:,feature]))
        if i <= 9:
            print('%s jaccard_similarity: ------ %+.3f %+.3f %+.3f %+.3f %+.3f %+.3f %+.3f %+.3f %+.3f %+.3f %+.3f' % ('sample%.2d' % (i), coeffs[0], coeffs[1], coeffs[2], coeffs[3], coeffs[4], coeffs[5], coeffs[6], coeffs[7], coeffs[8], coeffs[9], coeffs[10]))
        else:
            print('%s jaccard_similarity: %+.3f %+.3f %+.3f %+.3f %+.3f %+.3f %+.3f %+.3f %+.3f %+.3f %+.3f %+.3f' % ('sample%.2d' % (i), coeffs[0], coeffs[1], coeffs[2], coeffs[3], coeffs[4], coeffs[5], coeffs[6], coeffs[7], coeffs[8], coeffs[9], coeffs[10], coeffs[11]))
