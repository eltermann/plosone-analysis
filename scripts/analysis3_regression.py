# -*- coding: utf-8 -*-
from os.path import join, dirname
import sys
sys.path.append(join(dirname(__file__), '..'))

import utils

import argparse
from os.path import join

import numpy as np
from sklearn.linear_model import LogisticRegression

def coeff_if_nez(coeff):
    return '%+.2e' % coeff if coeff != 0.0 else '    0    '

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--samplesdir', required=True)
    args = parser.parse_args()

    sample_threshold = np.load(join(args.samplesdir, 'sample_threshold.pic'))
    y0_weight = 1.0 / sample_threshold
    y1_weight = 1.0
    print('                   bias    dist=1    dist=2    dist=3    dist=4    dist=5    dist=6    dist=7    dist=8    dist=9    dist=∞')
    for i in [1, 2, 3, 6, 7]:
        sampleid = 'sample{0}.pic'.format(i)
        data = np.load(join(args.samplesdir, sampleid))
        if not data.shape[0]:
            continue
        X = np.array([utils.dist2fv(x) for x in data[:, 0]])
        y = data[:, 1]
        sample_weight = y0_weight*(y==0) + y1_weight*(y==1)
        clf = LogisticRegression()
        clf.fit(X, y, sample_weight)
        print('%s coefs: %s %s %s %s %s %s %s %s %s %s' % ('sample%.2d' % (i), coeff_if_nez(clf.coef_[0]), coeff_if_nez(clf.coef_[1]), coeff_if_nez(clf.coef_[2]), coeff_if_nez(clf.coef_[3]), coeff_if_nez(clf.coef_[4]), coeff_if_nez(clf.coef_[5]), coeff_if_nez(clf.coef_[6]), coeff_if_nez(clf.coef_[7]), coeff_if_nez(clf.coef_[8]), coeff_if_nez(clf.coef_[9])))
        print('%s intercept: %s' % ('sample%.2d' % (i), clf.intercept_))
