import argparse
from os.path import join

import numpy as np


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--samplesdir', required=True)
    args = parser.parse_args()

    sample_threshold = np.load(join(args.samplesdir, 'sample_threshold.pic'))

    for i in range(2):
        complete_sample = np.load(join(args.samplesdir, 'sample%d.pic' % (9*i+1)))
        if complete_sample.shape[0] > 0:
            y_1_all = sum(complete_sample[:,1]==1)
            pairs_all = y_1_all + (sum(complete_sample[:,1]==0) / sample_threshold)
            print('Not considering geographical boundaries, %d in %d pairs feature knowledge flow (%.4f%%)' % (y_1_all, pairs_all, 100*y_1_all/pairs_all))

        same_first_countries_sample = np.load(join(args.samplesdir, 'sample%d.pic' % (9*i+2)))
        if same_first_countries_sample.shape[0] > 0:
            y_1_same_first_countries = sum(same_first_countries_sample[:,1]==1)
            pairs_same_first_countries = y_1_same_first_countries + (sum(same_first_countries_sample[:,1]==0) / sample_threshold)
            print('P(y=1 | the first countries are present and the same) = %.4f%%' % (100*y_1_same_first_countries/pairs_same_first_countries))
        different_first_countries_sample = np.load(join(args.samplesdir, 'sample%d.pic' % (9*i+3)))
        if different_first_countries_sample.shape[0] > 0:
            y_1_different_first_countries = sum(different_first_countries_sample[:,1]==1)
            pairs_different_first_countries = y_1_different_first_countries + (sum(different_first_countries_sample[:,1]==0) / sample_threshold)
            print('P(y=1 | the first countries are present and different) = %.4f%%' % (100*y_1_different_first_countries/pairs_different_first_countries))
        same_majority_countries_sample = np.load(join(args.samplesdir, 'sample%d.pic' % (9*i+4)))
        if same_majority_countries_sample.shape[0] > 0:
            y_1_same_majority_countries = sum(same_majority_countries_sample[:,1]==1)
            pairs_same_majority_countries = y_1_same_majority_countries + (sum(same_majority_countries_sample[:,1]==0) / sample_threshold)
            print('P(y=1 | the majority countries are present and the same) = %.4f%%' % (100*y_1_same_majority_countries/pairs_same_majority_countries))
        different_majority_countries_sample = np.load(join(args.samplesdir, 'sample%d.pic' % (9*i+5)))
        if different_majority_countries_sample.shape[0] > 0:
            y_1_different_majority_countries = sum(different_majority_countries_sample[:,1]==1)
            pairs_different_majority_countries = y_1_different_majority_countries + (sum(different_majority_countries_sample[:,1]==0) / sample_threshold)
            print('P(y=1 | the majority countries are present and different) = %.4f%%' % (100*y_1_different_majority_countries/pairs_different_majority_countries))
        same_first_regions_sample = np.load(join(args.samplesdir, 'sample%d.pic' % (9*i+6)))
        if same_first_regions_sample.shape[0] > 0:
            y_1_same_first_regions = sum(same_first_regions_sample[:,1]==1)
            pairs_same_first_regions = y_1_same_first_regions + (sum(same_first_regions_sample[:,1]==0) / sample_threshold)
            print('P(y=1 | the first regions are present and the same) = %.4f%%' % (100*y_1_same_first_regions/pairs_same_first_regions))
        different_first_regions_sample = np.load(join(args.samplesdir, 'sample%d.pic' % (9*i+7)))
        if different_first_regions_sample.shape[0] > 0:
            y_1_different_first_regions = sum(different_first_regions_sample[:,1]==1)
            pairs_different_first_regions = y_1_different_first_regions + (sum(different_first_regions_sample[:,1]==0) / sample_threshold)
            print('P(y=1 | the first regions are present and different) = %.4f%%' % (100*y_1_different_first_regions/pairs_different_first_regions))
        same_majority_regions_sample = np.load(join(args.samplesdir, 'sample%d.pic' % (9*i+8)))
        if same_majority_regions_sample.shape[0] > 0:
            y_1_same_majority_regions = sum(same_majority_regions_sample[:,1]==1)
            pairs_same_majority_regions = y_1_same_majority_regions + (sum(same_majority_regions_sample[:,1]==0) / sample_threshold)
            print('P(y=1 | the majority regions are present and the same) = %.4f%%' % (100*y_1_same_majority_regions/pairs_same_majority_regions))
        different_majority_regions_sample = np.load(join(args.samplesdir, 'sample%d.pic' % (9*i+9)))
        if different_majority_regions_sample.shape[0] > 0:
            y_1_different_majority_regions = sum(different_majority_regions_sample[:,1]==1)
            pairs_different_majority_regions = y_1_different_majority_regions + (sum(different_majority_regions_sample[:,1]==0) / sample_threshold)
            print('P(y=1 | the majority regions are present and different) = %.4f%%' % (100*y_1_different_majority_regions/pairs_different_majority_regions))
