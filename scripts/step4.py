import os
from os.path import join, dirname
import sys
sys.path.append(join(dirname(__file__), '..'))

import argparse
from bson.objectid import ObjectId
import igraph
import logging
import numpy as np
from scipy.sparse import csr_matrix

import settings
import utils

utils.configure_logging()


if __name__ == '__main__':
    """
    This script implements the following pseudo-code.

    g <- empty graph
    distances <- empty hashmap # feature vectors
    flow <- empty hashmap # dependent variables
    for each year _y_ taken in order:
        g.vertices <- g.vertices U (articles with year=_y_)
        for each article _a_ with year=_y_:
            for each article _b_ from g.vertices with _b_ != _a_:
                if _a_ and _b_ have at least one common author:
                    g.edges <- g.edges U {(_a_, _b_)}
        for each article _a_ with year=_y_: [1] [2]
            for each article _b_ from g.vertices with _b_ != _a_:
                distances[_a_, _b_] <- social_dist(_a_, _b_) [3]
                flow[_a_, _b_] <- 1 iff _a_ cites _b_ and there is no
                                    coauthor between _a_ and _b_ [4]

    [1] Since, in our dataset, _a_ will possibly cite _b_ iff _a_ is an article
        from PLoS One, we only consider these cases. Doing so, we avoid adding
        noisy pairs without knowledge flow.

    [2] Also, in order to perform control analysis, we sample the pairs as follows:
        sample 1: all pairs with y=1 + ~0.1% of pairs with y=0
        sample 2: only pairs with country_a == country_b
        sample 3: only pairs with country_a != country_b
        sample 4: the same for sample 2, but with different country definition
        sample 5: the same for sample 3, but with different country definition
        sample 6: only pairs with region_a == region_b
        sample 7: only pairs with region_a != region_b
        sample 8: the same for sample 6, but with different region definition
        sample 9: the same for sample 7, but with different region definition
        sample [10-18]: the same for [1-9] but with different flow definition

    [3] The social distance between two vertices is defined as the number of
        vertices between them in their shortest path.

    [4] We can relax Singh's definition of knowledge flow as follows:
        flow[_a_, _b_] <- 1 iff _a_ cites _b_ and there is at least one
                            author in _a_ that is not in _b_
    """
    mongodb = utils.mongodb_db()
    parser = argparse.ArgumentParser()
    parser.add_argument('--samplesdir', required=True)
    parser.add_argument('--plosoneratio', type=float, default=0.05)
    args = parser.parse_args()
    assert args.plosoneratio <= 1, 'Value error: PLOS ONE ratio > 1'
    if not os.path.exists(args.samplesdir):
        os.makedirs(args.samplesdir)
    years = sorted(mongodb.article.distinct('year'))
    citations_count = mongodb.article.aggregate([
        {'$match': {'references': {'$exists': 1}}},
        {'$group': {'_id': {}, 'count': {'$sum': {'$size': "$references"}}}},
    ]).next()['count']
    article_count = mongodb.article.count()
    sample_threshold = min(0.01, 500.0 * citations_count / (article_count ** 2))
    np.float64(sample_threshold).dump(join(args.samplesdir, 'sample_threshold.pic'))
    np.float64(args.plosoneratio).dump(join(args.samplesdir, 'plosoneratio.pic'))

    logging.info('Sampling threshold for y=0 pairs: %.10f', sample_threshold)

    g = igraph.Graph(directed=False)
    author_articles = {} # author - to -> articles index
    article_authors = {} # article - to -> authors index
    article_first_country = {} # article - to -> first_country index
    article_majority_country = {} # article - to -> majority_country index
    article_first_region = {} # article - to -> first_region index
    article_majority_region = {} # article - to -> majority_region index

    pairs_count = 0
    samples = {}
    for i in range(1, 19):
        samples[i] = []

    for year in years:
        if year < 1995:
            # reduce collaboration and spillover time-span
            continue

        logging.info(
            '%s: vertices=%s, edges=%s, pairs=%s', year, g.vcount(),
            g.ecount(), pairs_count
        )
        new_articles = list(mongodb.article.find({'year': year}))
        new_articles_total = len(new_articles)

        # add articles to graph
        g.add_vertices([str(a['_id']) for a in new_articles])

        # populate indexes
        for article in new_articles:
            articleid = str(article['_id'])
            article_first_country[articleid] = article.get('first_country')
            article_majority_country[articleid] = article.get('majority_country')
            article_first_region[articleid] = article.get('first_region')
            article_majority_region[articleid] = article.get('majority_region')
            for author in article.get('authors', []):
                authorid = str(author)
                author_articles.setdefault(authorid, set()).add(articleid)
                article_authors.setdefault(articleid, set()).add(authorid)

        # add edges to graph
        edges_to_add = []
        for a in new_articles:
            a_id = str(a['_id'])
            a_vertex = g.vs.find(a_id)
            a_authors = [str(i) for i in a['authors']]
            collaborations = set([art for aut in a_authors for art in author_articles[aut]])
            collaborations.remove(a_id)
            edges_to_add += [(a_vertex, g.vs.find(b_id)) for b_id in collaborations]
        g.add_edges(edges_to_add)

        if year < 2004 or year > 2016:
            # for lower years, just build the graph
            continue

        a_count = 0
        for a in new_articles:
            # extract social distances and flows of knowledge
            a_count += 1
            if a_count % 1000 == 0:
                logging.info(
                    '%s: (checkpoint) %d of %d (%.2f%%)', year, a_count,
                    new_articles_total, 100.0 * a_count / new_articles_total
                )
                for i in range(1, 19):
                    np.array(samples[i]).dump(join(args.samplesdir, u'sample{0}.pic'.format(i)))

            try:
                assert isinstance(a['references'], list)
            except:
                # there is no need to process an article without references
                continue

            if np.random.random() > args.plosoneratio:
                # sample a fraction of all available data
                continue

            a_id = str(a['_id'])
            a_vertex = g.vs.find(name=a_id)
            b_vertices = [] # destination vertices to calculate shortest_paths
            b_vertices_flow = {}
            for b_vertex in g.vs:
                if b_vertex.index == a_vertex.index:
                    continue
                pairs_count += 1
                b_id = b_vertex.attributes().get('name')
                in_references = ObjectId(b_id) in a['references']
                len_intersec = len(article_authors[a_id].intersection(article_authors[b_id]))
                singh_flow = in_references and len_intersec == 0
                our_flow = in_references and len(article_authors[b_id]) > len_intersec
                rand = np.random.random() < sample_threshold
                if singh_flow or our_flow or rand:
                    b_vertices.append(b_vertex)
                    b_vertices_flow[b_vertex.index] = {0: singh_flow, 1: our_flow}

            # compute shortest paths
            shortest_paths = g.get_shortest_paths(a_vertex, b_vertices, output='epath')

            for j in range(len(b_vertices)):
                b_vertex = b_vertices[j]
                b_id = b_vertex.attributes().get('name')
                social_dist = min(len(shortest_paths[j]) - 1, 120)
                for i in [0, 1]:
                    row = np.array([social_dist, b_vertices_flow[b_vertex.index][i]], dtype=np.int8)
                    samples[9*i + 1].append(row)
                    if article_first_country[a_id] and article_first_country[b_id]:
                        if article_first_country[a_id] == article_first_country[b_id]:
                            samples[9*i + 2].append(row)
                        else:
                            samples[9*i + 3].append(row)
                    if article_majority_country[a_id] and article_majority_country[b_id]:
                        if article_majority_country[a_id] == article_majority_country[b_id]:
                            samples[9*i + 4].append(row)
                        else:
                            samples[9*i + 5].append(row)
                    if article_first_region[a_id] and article_first_region[b_id]:
                        if article_first_region[a_id] == article_first_region[b_id]:
                            samples[9*i + 6].append(row)
                        else:
                            samples[9*i + 7].append(row)
                    if article_majority_region[a_id] and article_majority_region[b_id]:
                        if article_majority_region[a_id] == article_majority_region[b_id]:
                            samples[9*i + 8].append(row)
                        else:
                            samples[9*i + 9].append(row)

        for i in range(1, 19):
            logging.info('final sample %d persisted: %d rows', i, len(samples[i]))
            np.array(samples[i]).dump(join(args.samplesdir, u'sample{0}.pic'.format(i)))

    logging.info('Total pairs visited: %d', pairs_count)
