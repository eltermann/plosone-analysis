from os.path import join, dirname
import sys
sys.path.append(join(dirname(__file__), '..'))


import logging
from lxml import etree

import settings
import utils

utils.configure_logging()


if __name__ == '__main__':
    mongodb = utils.mongodb_db()
    logging.info('Iterating over corpus to load articles references into DB')
    count = 0
    for xml in utils.load_corpus():
        try:
            tree = etree.fromstring(xml)
        except Exception as e:
            logging.warning('Unable to parse document: %s', repr(e))
            continue

        # article
        doi = ''
        try:
            doi = tree.xpath(
                '/article/front/article-meta/article-id[@pub-id-type="doi"]'
            )[0].text.strip()
        except Exception as e:
            logging.warning('Unable to fetch DOI: %s', repr(e))
            continue

        existing_article = utils._existing_article(doi, None) # load by doi
        if not existing_article:
            logging.warning('Missing article %s', doi)
            continue

        # referred articles
        references = []
        for ref_article_n in tree.xpath(
            '/article/back/ref-list/ref/*[@publication-type="journal"]'
        ):
            ref_doi = ''
            try:
                assert 'doi' in ref_article_n.find('comment').text, 'malformed'
                ref_doi = ref_article_n.xpath(
                    './comment/ext-link[@ext-link-type="uri"]'
                )[0].text
            except Exception as e:
                logging.debug('Unable to find DOI (ref)')

            try:
                ref_title = ref_article_n.find('article-title').text
                assert ref_title, 'Title unavailable (ref)'
                ref_title_h = utils._title_hash(
                    utils._canonical_title(ref_title)
                )
            except Exception as e:
                logging.debug('Unable to fetch title (ref): %s', repr(e))
                if not ref_doi:
                    logging.warn('Unable to fetch title nor doi; skip (ref).')
                    continue

            existing_ref_article = utils._existing_article(ref_doi, ref_title_h)
            if existing_ref_article:
                references.append(existing_ref_article['_id'])
        count += len(references)
        mongodb.article.update_one(
            {'_id': existing_article['_id']},
            {'$set': {'references': references}}
        )

    logging.info('Created:')
    logging.info('   %d references', count)
