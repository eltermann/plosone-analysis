# -*- coding: utf-8 -*-
from os.path import join, dirname
import sys
sys.path.append(join(dirname(__file__), '..'))

import utils

import argparse
import numpy as np
from sklearn.linear_model import Ridge


def coeff_if_nez(coeff):
    return '%+.2e' % coeff if coeff != 0.0 else '    0    '


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--samplesdir', required=True)
    args = parser.parse_args()

    sample_threshold = np.load(join(args.samplesdir, 'sample_threshold.pic'))
    y0_weight = 1.0 / sample_threshold
    y1_weight = 1.0

    # from step 4, we got:
    # sample 2: only pairs with country_a == country_b
    # sample 3: only pairs with country_a != country_b
    print('Adding country variable:')
    data_same_country = np.load(join(args.samplesdir, 'sample2.pic'))
    data_diff_country = np.load(join(args.samplesdir, 'sample3.pic'))
    X_same_country = [np.concatenate((utils.dist2fv(x), [1]), axis=0) for x in data_same_country[:, 0]]
    X_diff_country = [np.concatenate((utils.dist2fv(x), [0]), axis=0) for x in data_diff_country[:, 0]]
    X = np.array(X_same_country + X_diff_country)
    y = np.concatenate((data_same_country[:, 1], data_diff_country[:, 1]), axis=0)
    sample_weight = y0_weight*(y==0) + y1_weight*(y==1)
    clf = Ridge(alpha=1.0)
    clf.fit(X, y, sample_weight)
    print('        dist=1    dist=2    dist=3    dist=4    dist=5    dist=6    dist=7    dist=8    dist=9    dist=∞    is_same_country')
    print('coefs: %s %s %s %s %s %s %s %s %s %s %s' % (coeff_if_nez(clf.coef_[0]), coeff_if_nez(clf.coef_[1]), coeff_if_nez(clf.coef_[2]), coeff_if_nez(clf.coef_[3]), coeff_if_nez(clf.coef_[4]), coeff_if_nez(clf.coef_[5]), coeff_if_nez(clf.coef_[6]), coeff_if_nez(clf.coef_[7]), coeff_if_nez(clf.coef_[8]), coeff_if_nez(clf.coef_[9]), coeff_if_nez(clf.coef_[10])))

    # from step 4, we got:
    # sample 6: only pairs with region_a == region_b
    # sample 7: only pairs with region_a != region_b
    print('Adding region variable:')
    data_same_region = np.load(join(args.samplesdir, 'sample6.pic'))
    data_diff_region = np.load(join(args.samplesdir, 'sample7.pic'))
    X_same_region = [np.concatenate((utils.dist2fv(x), [1]), axis=0) for x in data_same_region[:, 0]]
    X_diff_region = [np.concatenate((utils.dist2fv(x), [0]), axis=0) for x in data_diff_region[:, 0]]
    X = np.array(X_same_region + X_diff_region)
    y = np.concatenate((data_same_region[:, 1], data_diff_region[:, 1]), axis=0)
    sample_weight = y0_weight*(y==0) + y1_weight*(y==1)
    clf = Ridge(alpha=1.0)
    clf.fit(X, y, sample_weight)
    print('        dist=1    dist=2    dist=3    dist=4    dist=5    dist=6    dist=7    dist=8    dist=9    dist=∞    is_same_region')
    print('coefs: %s %s %s %s %s %s %s %s %s %s %s' % (coeff_if_nez(clf.coef_[0]), coeff_if_nez(clf.coef_[1]), coeff_if_nez(clf.coef_[2]), coeff_if_nez(clf.coef_[3]), coeff_if_nez(clf.coef_[4]), coeff_if_nez(clf.coef_[5]), coeff_if_nez(clf.coef_[6]), coeff_if_nez(clf.coef_[7]), coeff_if_nez(clf.coef_[8]), coeff_if_nez(clf.coef_[9]), coeff_if_nez(clf.coef_[10])))
