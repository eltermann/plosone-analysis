# -*- coding: utf-8 -*-
import argparse
from os.path import join

import numpy as np


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--samplesdir', required=True)
    args = parser.parse_args()

    for i in range(1, 19):
        data = np.load(join(args.samplesdir, 'sample%d.pic' % (i)))
        total_y_0 = 0
        total_y_1 = 0
        total_x__1 = 0
        total_x__2 = 0
        total_x_0 = 0
        total_x_1 = 0
        total_x_2 = 0
        total_x_3 = 0
        if data.shape[0] > 0:
            x = data[:,0]
            y = data[:,1]
            total_y_0 = sum(y==0)
            total_y_1 = sum(y==1)
            total_x__1 = sum(x==-1)
            total_x__2 = sum(x==-2)
            total_x_0 = sum(x==0)
            total_x_1 = sum(x==1)
            total_x_2 = sum(x==2)
            total_x_3 = sum(x==3)

        print('%s rows %.5d | y=0 %.5d; y=1 %.5d | x=0 %.5d; x=1 %.5d; x=2 %.5d; x=3 %.5d; x>=4 %.5d; x=∞ %.5d' % ('sample%.2d.pic' % (i), data.shape[0], total_y_0, total_y_1, total_x_0, total_x_1, total_x_2, total_x_3, total_x__1, total_x__2))
