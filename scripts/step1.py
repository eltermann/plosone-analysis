from os.path import join, dirname
import sys
sys.path.append(join(dirname(__file__), '..'))


import logging
from lxml import etree

import settings
import utils

utils.configure_logging()


if __name__ == '__main__':
    mongodb = utils.mongodb_db()
    logging.info
    mongodb.author.ensure_index([('canonical_name', 1)])
    mongodb.subject.ensure_index([('name', 1)])
    authors_count = 0
    referred_authors_count = 0
    subjects_count = 0
    logging.info('Iterating over corpus to load authors and subjects into DB')
    for xml in utils.load_corpus():
        try:
            tree = etree.fromstring(xml)
        except Exception as e:
            logging.warning('Unable to parse document: %s', repr(e))
            continue

        # article authors
        for author_node in tree.xpath('//contrib[@contrib-type="author"]'):
            try:
                name_node = author_node.find('name')
                surname, given_names = utils._extract_author_names(name_node)
                assert surname and given_names, 'Invalid author name'
                name = utils._author_canonical_name(surname, given_names)
            except Exception as e:
                logging.warning(repr(e))
                continue

            existing_author = mongodb.author.find_one({'canonical_name': name})
            if not existing_author:
                author_doc = {'canonical_name': name}
                mongodb.author.insert(author_doc)
                logging.debug(u'Created author %s', name)
                authors_count += 1
                existing_author = author_doc

            try:
                rid = author_node.xpath('./xref[@ref-type="aff"]')[0].get('rid')
                address_line = tree.xpath(
                    u'//article-meta/aff[@id="{0}"]/addr-line'.format(rid)
                )[0].text
                address_line_parts = address_line.split(',')
                country = unicode(address_line_parts[-1].upper().strip())
                region = u'{0}::{1}'.format(
                    country,
                    address_line_parts[-2].upper().strip()
                )
            except Exception as e:
                logging.warning(u'Author address line: %s', repr(e))
                pass
            else:
                existing_author['country'] = country
                existing_author['region'] = region
                mongodb.author.update_one(
                    {'_id': existing_author['_id']},
                    {'$set': {'country': country, 'region': region}}
                )
                logging.debug(u'Set author address %s', region)

        # referred authors
        for name_node in tree.xpath(
            '/article/back/ref-list/ref/*[@publication-type="journal"]'
            '//name[@name-style="western"]'
        ):
            try:
                surname, given_names = utils._extract_author_names(name_node)
                assert surname and given_names, 'Invalid author name'
                name = utils._author_canonical_name(surname, given_names)
            except Exception as e:
                logging.warning(u'Referred author: %s', repr(e))
                continue

            existing_author = mongodb.author.find_one({'canonical_name': name})
            if not existing_author:
                author_doc = {'canonical_name': name}
                mongodb.author.insert(author_doc)
                logging.debug(u'Created author %s', name)
                referred_authors_count += 1

        # subjects
        # TODO - just found that PLOS ONE categories change over time
        # E.g: journal.pone.0000031.xml doesn't have "Discipline-v3" but it has
        # "Discipline" instead (with a different organization yay!)
        for subject_group_node in tree.xpath(
            '//article-meta/article-categories'
            '/subj-group[@subj-group-type="Discipline-v3"]'
        ):
            level = 1
            last_parent = None
            last_names = []
            for subject_name_n in subject_group_node.iterdescendants('subject'):
                name = subject_name_n.text.upper()
                last_names.append(name)
                name = '::'.join(last_names)
                subject_doc = mongodb.subject.find_one({
                    'name': name,
                    'level': level,
                })
                if not subject_doc:
                    subject_doc = {
                        'name': name,
                        'level': level,
                        'parent': last_parent,
                    }
                    mongodb.subject.insert(subject_doc)
                    logging.debug(u'Created subject %s', name)
                    subjects_count += 1
                level += 1
                last_parent = subject_doc['_id']

    logging.info('Created:')
    logging.info('   %d authors', authors_count)
    logging.info('   %d referred authors', referred_authors_count)
    logging.info('   %d subjects', subjects_count)
