from os.path import join, dirname
import sys
sys.path.append(join(dirname(__file__), '..'))


import logging
from lxml import etree

import settings
import utils

utils.configure_logging()


if __name__ == '__main__':
    mongodb = utils.mongodb_db()
    for doc in mongodb.article.aggregate([
        {'$match': {'year': {'$gt': 1960, '$lt': 2016}}},
        {'$group': {'_id': '$year', 'num_articles': {'$sum': 1}, 'num_authors': {'$sum': {'$size': '$authors'}}}},
        {'$sort': {'_id': 1}}
    ]):
        year = doc['_id']
        num_articles = doc['num_articles']
        avg_authors_per_article = doc['num_authors'] / float(num_articles)
        logging.info(u'Authors: %s: %s articles with an avg of %.1f authors/article' % (
            year, num_articles, avg_authors_per_article
        ))

    for doc in mongodb.article.aggregate([
        {'$match': {'year': {'$gt': 1960, '$lt': 2016}, 'doi': {'$regex': 'journal\.pone'}}},
        {'$group': {'_id': '$year', 'num_articles': {'$sum': 1}, 'num_authors': {'$sum': {'$size': '$authors'}}}},
        {'$sort': {'_id': 1}}
    ]):
        year = doc['_id']
        num_articles = doc['num_articles']
        avg_authors_per_article = doc['num_authors'] / float(num_articles)
        logging.info(u'Authors: %s: %s PLOS ONE articles with an avg of %.1f authors/article' % (
            year, num_articles, avg_authors_per_article
        ))

    for doc in mongodb.article.aggregate([
        {'$match': {'year': {'$gt': 1960, '$lt': 2016}, 'references': {'$exists': 1}, 'doi': {'$regex': 'journal\.pone'}}},
        {'$group': {'_id': '$year', 'num_articles': {'$sum': 1}, 'num_refs': {'$sum': {'$size': '$references'}}}},
        {'$sort': {'_id': 1}}
    ]):
        year = doc['_id']
        num_articles = doc['num_articles']
        avg_citations_per_article = doc['num_refs'] / float(num_articles)
        logging.info(u'Citations: %s: %s citing PLOS ONE articles with an avg of %.1f citations/citing article' % (
            year, num_articles, avg_citations_per_article
        ))
