import hashlib
import logging
import numpy as np
import pymongo
import re

import settings


def configure_logging():
    if hasattr(settings, 'LOGGING_LEVEL'):
        logging_level = settings.LOGGING_LEVEL
    else:
        logging_level = logging.INFO
    logging.basicConfig(
        format='%(asctime)s : %(levelname)s : %(message)s', level=logging_level
    )


def load_corpus():
    return settings.CORPUS_ITERATOR(
        *settings.CORPUS_ITERATOR_ARGS,
        **settings.CORPUS_ITERATOR_KWARGS
    )


def mongodb_db(refresh=False):
    if not hasattr(mongodb_db, 'cli') or refresh:
        mongodb_db.cli = pymongo.MongoClient(
            *settings.MONGODB_CLIENT_ARGS,
            **settings.MONGODB_CLIENT_KWARGS
        )
    return mongodb_db.cli[settings.MONGODB_DATABASE]


def _extract_author_names(name_node):
    surname = name_node.xpath('./surname')[0].text
    given_names = name_node.xpath('./given-names')[0].text
    return surname, given_names


"""
Name conversion:
'Baird::Donald J.' -> 'BAIRD::DJ'
'Bais::Manish' -> 'BAIS::M'
'van den Berg::Leonard H.' -> 'VAN DEN BERG::LH'
'Whipple::FJW' -> 'WHIPPLE::FJW'
"""
def _author_canonical_name(surname, given_names):
    _surname = surname.upper().strip()
    _given_names = ''.join([char for char in given_names if char.isupper()])
    return u'{0}::{1}'.format(_surname, _given_names)


"""
Title conversion:
'This is an example' -> 'THIS IS AN EXAMPLE'
'This one contains  multiple  spaces' -> 'THIS ONE CONTAINS MULTIPLE SPACES'
'And this, non-letter chars (2)' -> 'AND THIS NON LETTER CHARS'
"""
def _canonical_title(title):
    title = title.upper()
    # replace multiple spaces and non-letter characters by one space
    title = re.sub('[^A-Z]', ' ', title)
    title = re.sub('\ +', ' ', title)
    title = title.strip()
    return title


def _title_hash(canonical_title):
    assert canonical_title, 'Do not compute hash on empty title'
    return hashlib.sha1(canonical_title).hexdigest()


def _existing_article(doi, title_hash):
    mongodb = mongodb_db()
    if doi:
        doc = mongodb.article.find_one({'doi': doi})
        if doc:
            return doc
    if title_hash:
        doc = mongodb.article.find_one({'title_hash': title_hash})
        if doc:
            return doc
    return None


def _majority(lst):
    return max(
        ((item, lst.count(item)) for item in set(lst)), key=lambda a: a[1]
    )[0]


def dist2fv(social_dist):
    """
    dist: fv
     +1: [1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
     +2: [0, 1, 0, 0, 0, 0, 0, 0, 0, 0]
     ...
     +9: [0, 0, 0, 0, 0, 0, 0, 0, 1, 0]
     -1: [0, 0, 0, 0, 0, 0, 0, 0, 0, 1] (infinity distance)
    """
    fv = np.zeros(10, dtype=np.uint8)
    if social_dist == -1:
        fv[-1] = 1
    elif social_dist >= 1 and social_dist <= 9:
        fv[social_dist - 1] = 1
    return fv
