import glob
import logging
import os


class CorpusIterator(object):
    count = None

    def __init__(self, *args, **kwargs):
        self.info_step = kwargs.get('info_step', 100)

    def __iter__(self):
        return self

    def __next__(self):
        if self._i % self.info_step == 0 or self._i == self.count - 1:
            percent = 100.0 * (self._i + 1) / self.count
            logging.info('Reading doc %d (%.2f%%)', self._i + 1, percent)
        return self.next()

    def next(self):
        raise NotImplementedError('`next` method not implemented')


class FilesIterator(CorpusIterator):
    def __init__(self, corpus_dir, files_pattern, *args, **kwargs):
        super(FilesIterator, self).__init__(*args, **kwargs)
        if not os.path.isdir(corpus_dir):
            raise ValueError('Invalid corpus dir: {0}'.format(corpus_dir))
        self.corpus_dir = corpus_dir
        self._pattern = os.path.join(corpus_dir, files_pattern)
        self._files = glob.glob(self._pattern)
        if len(self._files) == 0:
            raise ValueError('No files for pattern: {0}'.format(self._pattern))
        self.count = len(self._files)
        logging.info('Corpus: identified %d documents', self.count)
        self._i = 0

    def next(self):
        if self._i < self.count:
            f = open(self._files[self._i])
            self._i += 1
            content = f.read()
            f.close()
            #content = content.decode('utf-8') # ensure unicode
            return content
        else:
            raise StopIteration()
