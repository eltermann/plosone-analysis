# PLOS ONE - collaboration network analysis #

Build a database of PLOS ONE articles and authors (and its cited articles and authors). Analyse the effect of collaboration on knowledge flow.

**Important:** this software does not handle the download of PLOS ONE raw content. You have to provide it the corpus of XML files.

### Requirements ###

* python >2.7
* mongodb >3.0


### How do I get set up? ###

```
#!bash
pip install -r REQUIREMENTS.txt
```

### Configuration ###

```
#!bash
cp settings.py.example settings.py
# override settings.py content
```

Configuration variables:

* LOGGING_LEVEL: defines verbosity level
* CORPUS_ITERATOR: class that load raw data into scripts
* CORPUS_ITERATOR_ARGS: arguments to corpus iterator (**edit for reading your XML files**)
* CORPUS_ITERATOR_KWARGS: keyword-based arguments to corpus iterator
* MONGODB_CLIENT_ARGS: mongodb basic configuration (**edit for your mongodb setup**)
* MONGODB_CLIENT_KWARGS: mongodb additional configuration
* MONGODB_DATABASE: mongodb name of database to use


### Step 1 ###
```
#!bash
python scripts/step1.py
```

Iterate over XML corpus to build {author} and {subject} collections.

Step 1 requires:

* configured corpus on settings.py
* configured mongodb instance on settings.py

Step 1 output:

* author MongoDB collecion
* subject MongoDB collection


### Step 2 ###

```
#!bash
python scripts/step2.py
```

Iterate over XML corpus to build {article} collection.

Step 2 requires:

* authors and subjects collections (outcome of step 1)
* configured corpus on settings.py
* configured mongodb instance on settings.py

Step 2 output:

* article MongoDB collection


### Step 3 ###

```
#!bash
python scripts/step3.py
```
Iterate over XML corpus to populate the {article}.references field.

Step 3 requires:

* article, authors and subjects collections (outcome of step 2)
* configured corpus on settings.py
* configured mongodb instance on settings.py

Step 3 output:

* enrichment of article MongoDB collection


### Step 4 ###

```
#!bash
python scripts/step4.py --samplesdir="/path/to/dir/to/store/samples"
```

Iterate over database. For each pair of articles, build the social distance feature vector, the dependent variable, and other information for analysis.

Step 4 requires:

* article, authors and subjects collections (outcome of step 3)
* configured mongodb instance on settings.py
* a directory to be provided in the command line, to store samples

Step 4 output:

* prepared data samples to be analysed


### Analysis ###

```
#!bash
python scripts/analysis1_scatter.py --samplesdir="/path/to/dir/to/store/samples"
python scripts/analysis2_pearson.py --samplesdir="/path/to/dir/to/store/samples"
python scripts/analysis3_regression.py --samplesdir="/path/to/dir/to/store/samples"
python scripts/analysis4_db_stats.py --samplesdir="/path/to/dir/to/store/samples"
python scripts/analysis5_samples_dist.py --samplesdir="/path/to/dir/to/store/samples"
python scripts/analysis6_effect_of_geography.py --samplesdir="/path/to/dir/to/store/samples"

```